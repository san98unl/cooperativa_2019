from django import forms
from apps.modelo.models import Cliente
from apps.modelo.models import Cuenta
from apps.modelo.models import Transaccion
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField


#por cada metodo se coloca un url..
class FormularioCliente(forms.ModelForm):
	class Meta:
		model = Cliente
		fields=["cedula","nombres","apellidos","fechaNacimiento",
		"genero","estadocivil","correo","telefono","celular",
		"direccion"]#lo que voy a representar en la parte grafica
		widgets = {
		'cedula' : forms.TextInput(attrs={'class':'form-control'}),
		'nombres' : forms.TextInput(attrs={'class':'form-control'}),
		'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
		'fechaNacimiento' :forms.TextInput(attrs={'class':'form-control'}),
		'genero' : forms.Select(attrs={'class':'form-control'}),
		'estadocivil' : forms.Select(attrs={'class':'form-control'}),
		'correo' : forms.TextInput(attrs={'class':'form-control'}),
		'telefono' : forms.TextInput(attrs={'class':'form-control'}),
		'celular' : forms.TextInput(attrs={'class':'form-control'}),
		'direccion' : forms.TextInput(attrs={'class':'form-control'}),
		}
class FormularioModificarCliente(forms.ModelForm):
	class Meta:
		model = Cliente
		fields=["nombres","apellidos","fechaNacimiento",
		"genero","estadocivil","correo","telefono","celular",
		"direccion"]#lo que voy a representar en la parte grafica
class FormularioCuenta(forms.ModelForm):
	class Meta:
		model = Cuenta
		fields=["numero","tipoCuenta","saldo"]
		widgets = {
		'numero' : forms.TextInput(attrs={'class':'form-control'}),
		'tipoCuenta' : forms.Select(attrs={'class':'form-control'}),
		'saldo' : forms.TextInput(attrs={'class':'form-control'}),
		
		}

class FormularioTransaccion(forms.ModelForm):
	class Meta:
		model = Transaccion
		fields=["valor","descripcion"]


from django.urls import path
from .import views


urlpatterns = [
 
    path('',views.principal, name="cliente"),
    path('crear_cliente',views.crear),
    path('modificar_cliente/',views.modificar),
    path('modificar_cuenta/',views.modificar_cuenta),
    path('eliminar_cliente/',views.eliminar),
    path('listar_cuenta/',views.listar_cuenta),
    path('crear_cuenta/',views.crear_cuenta),
    path(r'^deposito/(?P<numero>\d+)/$',views.depositar, name='deposito'),
    path(r'^retiro/(?P<numero>\d+)/$',views.retiro, name='retiro'),
]

from django.shortcuts import render, redirect
from .forms import FormularioCliente
from .forms import FormularioCuenta
from .forms import FormularioModificarCliente
from .forms import FormularioTransaccion
from django.contrib.auth.decorators import login_required
from apps.modelo.models import Cliente,Cuenta,Transaccion
#importae numeros aleatorios
from django.utils.crypto import get_random_string
import random

# accedemos al  url. y cambiamos el nombre del dominio, maneja la direccion y camalziza con el nombre
@login_required
def principal(request):
	lista=Cliente.objects.all()#elemnto del orm objetss.all oltengo los elementos y los almaceno en lista
	lista_cuentas=Cuenta.objects.all()
	context ={
		'lista' : lista,
		'lista_cuenta' : lista_cuentas,

	}

	return render (request,'cliente/principal_cliente.html',context)#reder redireccion, rempitanta un docuemto html en forma grafica,,,escribimos el nombre de nuestra url

#permite el emtodo guardar los datos  en una bd, tambienla direccion para poder navegar
@login_required
def crear(request):
	formulario = FormularioCliente(request.POST)
	formularioCuenta = FormularioCuenta(request.POST)	
	usuario = request.user#peticion es procesada x el framework agrega el usuario
	if usuario.groups.filter(name ='administrativo').exists(): 
		
		if request.method == 'POST':
			if formulario.is_valid() and formularioCuenta.is_valid():
				datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
				cliente=Cliente() #asi se crear los objetos en python
				cliente.cedula=datos.get('cedula')
				cliente.nombres=datos.get('nombres')
				cliente.apellidos=datos.get('apellidos')
				cliente.genero=datos.get('genero')
				cliente.estadocivil=datos.get('estadocivil')
				cliente.fechaNacimiento=datos.get('fechaNacimiento')
				cliente.correo=datos.get('correo')
				cliente.telefono=datos.get('telefono')
				cliente.celular=datos.get('celular')
				cliente.direccion=datos.get('direccion')
				cliente.save()
				datosCuenta=formularioCuenta.cleaned_data #Obteniendo datos del formulario cuenta
				cuenta =Cuenta()
				cuenta.numero = "7096"+repr(random.randint(1,1000000))
				cuenta.estado = True
				cuenta.fechaApertura = datosCuenta.get('fechaApertura')
				cuenta.tipoCuenta = datosCuenta.get('tipoCuenta')
				cuenta.saldo = datosCuenta.get('saldo')
				cuenta.Cliente = cliente
				cuenta.save();

				return redirect(principal)
	else:
			return render (request,'cliente/acceso_prohibido.html')	
			context={
			'f':formulario,
			'fc':formularioCuenta,

			}
	return render (request,'cliente/crear_cliente.html', context)
@login_required
def modificar(request):
	dni = request.GET['cedula']
	cliente = Cliente.objects.get(cedula = dni)
	if request.method == 'POST': 
		formulario = FormularioModificarCliente(request.POST)
		if formulario.is_valid():
			datos=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
			cliente.nombres=datos.get('nombres')
			cliente.apellidos=datos.get('apellidos')
			cliente.genero=datos.get('genero')
			cliente.estadocivil=datos.get('estadocivil')
			cliente.fechaNacimiento=datos.get('fechaNacimiento')
			cliente.correo=datos.get('correo')
			cliente.telefono=datos.get('telefono')
			cliente.celular=datos.get('celular')
			cliente.direccion=datos.get('direccion')
			cliente.save()
			return redirect(principal)

		#se modifica
		
	else:
		formulario = FormularioCliente(instance = cliente)
	context = {
		'dni': dni,
		'cliente' : cliente,
		'formulario': formulario
	}
	return render (request,'cliente/modificar_cliente.html', context)
	#zmodificar cuenta
@login_required
def modificar_cuenta(request):
	dni = request.GET['numero']
	cuenta = Cuenta.objects.get(numero = dni)
	if request.method == 'POST': 
		formularioCuenta = FormularioModificarCuenta(request.POST)
		if formularioCuenta.is_valid():
			datos_cuenta=formulario.cleaned_data #obteniendo todos los datos del formulario cliente
			cuenta.numero=datos_cuenta.get('numero')
			cuenta.tipoCuenta=datos_cuenta.get('tipoCuenta')
			cuenta.saldo=datos.get('saldo')
			
			cuenta.save()
			return redirect(principal)

		#se modifica
		
	else:
		formularioCuenta = FormularioCuenta(instance = cuenta)
	context = {
		'dni': dni,
		'cuenta' : cuenta,
		'formulario_cuenta': formularioCuenta
	}
	return render (request,'cuenta/modificar_cuenta.html', context)
#eliminar cliente
@login_required
def eliminar(request):
	dni = request.GET['cedula']
	cliente = Cliente.objects.get(cedula = dni)
	cliente.delete()

	return redirect(principal)
#listar cuenta
@login_required
def listar_cuenta(request):
	dni = request.GET['cedula']
	cliente = Cliente.objects.get(cedula = dni)
	cuentas=Cuenta.objects.filter(cliente_id = cliente.cliente_id)


	return render(request,'cuenta/listar_cuentas.html',{'lista_c': cuentas})
#crear cuenta
@login_required
def crear_cuenta(request):
	dni = request.GET['cedula']
	cliente = Cliente.objects.get(cedula = dni)
	
	formularioCuenta = FormularioCuenta(request.POST)
	
	if request.method == 'POST':
		if formularioCuenta.is_valid():
			
			datosCuenta=formularioCuenta.cleaned_data #Obteniendo datos del formulario cuenta
			cuenta =Cuenta()
			cuenta.numero = "7096"+repr(random.randint(1,1000000))
			cuenta.estado = True
			cuenta.fechaApertura = datosCuenta.get('fechaApertura')
			cuenta.tipoCuenta = datosCuenta.get('tipoCuenta')
			cuenta.saldo = datosCuenta.get('saldo')
			cuenta.cliente_id = cliente.cliente_id
			cuenta.save();

			return redirect(principal)

	context={
		
		'fc':formularioCuenta,

	}
	return render (request,'cliente/crear_cliente.html', context)
#adepositar
@login_required
def depositar(request,numero):
	cuenta = Cuenta.objects.get(numero=numero)
	formularioT=FormularioTransaccion(request.POST)

	cliente = Cliente.objects.get(cliente_id = cuenta.cliente_id)	
	if request.method == 'POST':
		if formularioT.is_valid():
			datos=formularioT.cleaned_data
			cuenta.saldo= cuenta.saldo+datos.get('valor')
			cuenta.save()
			transaccion=Transaccion()
			
			transaccion.tipo = 'deposito';
			transaccion.valor = datos.get('valor');
			transaccion.descripcion = datos.get('descripcion')
			transaccion.responsable = 'xxxxxxxxxxxxxxx'
			transaccion.cuenta =cuenta
			transaccion.save()
			mensaje='Transaccion exitosa'
			return render(request,'transaccion/transaccion_status.html',locals())

	context={
		'f':formularioT,
		'cuenta':cuenta,
		'cliente': cliente
	}
	return render(request,'transaccion/depositar.html',context)
#retirar
@login_required
def retiro(request,numero):
	cuenta = Cuenta.objects.get(numero=numero)
	formularioT=FormularioTransaccion(request.POST)

	cliente = Cliente.objects.get(cliente_id = cuenta.cliente_id)	
	if request.method == 'POST':
		if formularioT.is_valid():
			datos=formularioT.cleaned_data
			cuenta.saldo= cuenta.saldo-datos.get('valor')
			cuenta.save()
			transaccion=Transaccion()
			
			transaccion.tipo = 'retiro';
			transaccion.valor = datos.get('valor');
			transaccion.descripcion = datos.get('descripcion')
			transaccion.responsable = 'xxxxxxxxxxxxxxx'
			transaccion.cuenta =cuenta
			transaccion.save()
			mensaje='Transaccion exitosa'
			return render(request,'transaccion/transaccion_status.html',locals())

	context={
		'f':formularioT,
		'cuenta':cuenta,
		'cliente': cliente
	}
	return render(request,'transaccion/retiro.html',context)
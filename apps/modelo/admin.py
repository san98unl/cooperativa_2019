from django.contrib import admin

# Register your models here.
from.models import Cliente#realizar importaciones
from.models import Cuenta#realizar importaciones
from.models import Transaccion#realizar importaciones

class AdminCliente(admin.ModelAdmin):
	list_display = ["cedula","apellidos","nombres","genero"]#permite realizar listas
	list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["genero","estadocivil"]#son filtros
	search_fields=["cedula","apellidos"]#realiza las busquedas de los datos
	class Meta: #caracteristicas para mapear un modlo del crug
		model= Cliente
admin.site.register(Cliente, AdminCliente)

class AdminCuenta(admin.ModelAdmin):
	list_display = ["numero","estado","cliente"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["cliente_id"]#son filtros
	search_fields=["numero"]

	class Meta: #caracteristicas para mapear un modlo del crug
		model= Cuenta
admin.site.register(Cuenta, AdminCuenta)
	
class AdminTransaccion(admin.ModelAdmin):
	list_display = ["valor","descripcion","responsable"]#permite realizar listas
	#list_editable = ["apellidos","nombres"] #permite editar
	list_filter=["responsable"]#son filtros
	search_fields=["responsable"]#realiza las busquedas de los datos
	class Meta: #caracteristicas para mapear un modlo del crug
		model= Transaccion
admin.site.register(Transaccion, AdminTransaccion)

	
		

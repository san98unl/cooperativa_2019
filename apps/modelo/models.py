from django.db import models

# Create your models here.
class Cliente(models.Model):
	listaGenero=(
		('femenino','Femenino'),
		('masculino','Masculino'),#lista de generos para cargar el combo
		)
	listaEstadoCivil=(
		('soltero','Soltero'),
		('casado','Casado'),	
		('divorciado','Divorciado'),
		('viudo','Viudo'),
		)
	cliente_id = models.AutoField(primary_key = True)#es el auto incrementable
	cedula = models.CharField(max_length = 10, unique = True, null = False)
	nombres = models.CharField(max_length = 50, null = False)
	apellidos = models.CharField(max_length = 50, null = False)
	direccion = models.TextField(max_length = 50, default='sin direccion')
	telefono = models.CharField(max_length = 10)
	genero = models.CharField(max_length = 15, choices = listaGenero, default = 'Femenino' , null = False)
	estadocivil= models.CharField(max_length = 15,choices = listaEstadoCivil, default = 'Soltero', null=False)
	correo = models.EmailField(max_length = 50, null = False)
	fechaNacimiento = models.DateField(auto_now=False, auto_now_add = False, null = False)
	celular = models.CharField(max_length = 10)
	telefono =models.CharField(max_length = 13)

class Cuenta(models.Model):
	listaTipo=(
		('corriente','Corriente'),
		('ahorros','Ahorros'),
	)
	cuenta_id = models.AutoField(primary_key = True)
	numero = models.CharField(max_length = 20, unique=True, null= False)
	estado = models.BooleanField(default = True)
	fechaApertura = models.DateField(auto_now_add=True, null = False)
	tipoCuenta = models.CharField(max_length = 30, choices = listaTipo, null = False, default='ahorros')
	saldo = models.DecimalField(max_digits=10, decimal_places=3, null = False)
	cliente=models.ForeignKey('Cliente',default=1,verbose_name="Cliente",on_delete=models.CASCADE)
	#cliente = models.ForeignKey(
	#	'Cliente',#referencia al modelo
	#	on_delete=models.CASCADE,
	#	)
	def _str_(self): 
		string=str(self.saldo)+";"+str(self.cuenta_id)
		return string

class Transaccion(models.Model):
	listaTipoT=(
		('retiro','Retiro'),
		('deposito','Deposito'),
		('transferecia','Transferecia'),
	)

	transaccion_id = models.AutoField(primary_key = True)
	fecha = models.DateField(auto_now_add = True, null = False)
	tipo = models.CharField(max_length = 30, choices=listaTipoT, null = False,default=1)
	valor = models.DecimalField(max_digits = 10, decimal_places = 3,null = False)
	descripcion = models.TextField(null = False)
	responsable = models.CharField(max_length = 160, null = False)
	#presentar=models.ForeignKey(Cuenta,default=None)
	cuenta = models.ForeignKey(
		'Cuenta', default=1,
		on_delete=models.CASCADE,



	)

	
	
